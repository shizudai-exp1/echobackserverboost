/*
 * EchoBackServer.cpp
 *
 *  Created on: 2016/03/07
 *      Author: yasuh
 */
#include "EchoBackServer.hpp"
#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>

using namespace std;
using namespace boost::asio;

EchoBackServer::EchoBackServer() {
}

EchoBackServer::~EchoBackServer() {
}

bool EchoBackServer::start(uint16_t port) {

	try {
		io_service io_service;
		ip::tcp::acceptor acceptor(io_service,
				ip::tcp::endpoint(ip::tcp::v4(), port));

		while (true) {
			ip::tcp::socket socket(io_service);
			acceptor.accept(socket);

			// show remote host
			ip::tcp::endpoint ep = socket.remote_endpoint();
			ip::address remote_address = ep.address();
			unsigned int remote_port = ep.port();
			cout << "accept: " << remote_address.to_string() << ":" << remote_port << endl;

			boost::array<char, 256> recv_message;
			boost::system::error_code error;
			size_t size = socket.read_some(buffer(recv_message), error);

			while (error != boost::asio::error::eof) {
				if (error) {
					throw boost::system::system_error(error);
				}

				// console out
				cout.write(recv_message.data(), size);
				cout.flush();

				// echo back to the client
				socket.write_some(buffer(recv_message, size));
				size = socket.read_some(buffer(recv_message), error);
			}
			socket.close();
		}
	} catch (std::exception& e) {
		cerr << e.what() << endl;
	}
	return true;
}
