//============================================================================
// Name        : Main.cpp
// Author      : Yasuhiro Noguchi
// Version     : 0.0.1
// Copyright   : GPLv2
// Description : EchoBackServer
//============================================================================
#include <iostream>
using namespace std;

#include <boost/asio.hpp>
using namespace boost::asio;

#include "EchoBackServer.hpp"

int main(int argc, char** argv) {

	EchoBackServer s;
	s.start(8000);

	return 0;
}
