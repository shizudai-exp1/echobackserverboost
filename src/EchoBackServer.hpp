/*
 * EchoBackServer.hpp
 *
 *  Created on: 2016/03/07
 *      Author: yasuh
 */

#ifndef ECHOBACKSERVER_HPP_
#define ECHOBACKSERVER_HPP_

#include <stdint.h>
using namespace std;

class EchoBackServer {

public:
	EchoBackServer();
	~EchoBackServer();
	bool start(uint16_t port);
};

#endif /* ECHOBACKSERVER_HPP_ */
